package org.company.app.rule;

import junit.framework.TestCase;
import org.company.app.model.Journey;
import org.company.app.model.JourneyTime;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Time based rule tests.
 */
public class TimeRuleTest extends TestCase {
    /**
     * Single journey rule.
     */
    @Test
    public void testRulesBlank() {
        TimeRule rule = new TimeRule();
        JourneyTime journeyTime = new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        Journey journey = new Journey("Zone 1", "Zone 1", journeyTime);
        rule.applyRule(journey);
        assertFalse(journey.getJourneyTime().isPeakHour());
    }

    /**
     * Single journey rule.
     */
    @Test
    public void testRulesWithMorningWeekPeak() {
        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        TimeRule rule = new TimeRule(duration);
        JourneyTime journeyTime = new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        Journey journey = new Journey("Zone 1", "Zone 1", journeyTime);
        rule.applyRule(journey);
        assertTrue(journey.getJourneyTime().isPeakHour());
    }


}