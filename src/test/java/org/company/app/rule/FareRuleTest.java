package org.company.app.rule;

import junit.framework.TestCase;
import org.company.app.Fares;
import org.company.app.model.FareRate;
import org.company.app.model.FareTimeType;
import org.company.app.model.Journey;
import org.company.app.model.JourneyTime;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalTime;

/**
 * Fare based rule tests.
 */
public class FareRuleTest extends TestCase {

    /**
     * Single journey rule blank fares.
     */
    @Test
    public void testBlankFaresRules() {
        Fares fares = new Fares();
        FareRate fareRate = new FareRate();
        fareRate.setFare(FareTimeType.PEAK, 30);
        fareRate.setFare(FareTimeType.NON_PEAK, 25);
        fares.addFare("Zone 1", "Zone 1", fareRate);
        FareRule fareRule = new FareRule(fares);

        JourneyTime journeyTime = new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        Journey journey = new Journey("Zone 1", "Zone 1", journeyTime);

        fareRule.applyRule(journey);
        assertEquals(25, journey.getFare());
    }

    /**
     * Single journey rule peak fares.
     */
    @Test
    public void testPeakFaresRules() {
        Fares fares = new Fares();
        FareRate fareRate = new FareRate();
        fareRate.setFare(FareTimeType.PEAK, 30);
        fareRate.setFare(FareTimeType.NON_PEAK, 25);
        fares.addFare("Zone 1", "Zone 1", fareRate);
        FareRule fareRule = new FareRule(fares);

        JourneyTime journeyTime = new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        journeyTime.setPeakHour(true);
        Journey journey = new Journey("Zone 1", "Zone 1", journeyTime);

        fareRule.applyRule(journey);
        assertEquals(30, journey.getFare());
    }
}