package org.company.app;

import junit.framework.TestCase;
import org.company.app.model.*;
import org.company.app.rule.*;
import org.junit.Test;

import java.time.*;
import java.time.temporal.WeekFields;
import java.util.*;

/**
 * Unit test for updated card App.
 */
public class CardFareTest extends TestCase {

    /**
     * E2E Test ,  normal flow for 5 entries through loop for a day
     */
    public void testRuleForAJourney() {
        Fares fares = new Fares();
        FareRate fareRate = new FareRate();
        fareRate.setFare(FareTimeType.PEAK, 30);
        fareRate.setFare(FareTimeType.NON_PEAK, 25);
        fares.addFare("Zone 1", "Zone 1", fareRate);

        FareRule fareRule = new FareRule(fares);

        List<IRule> rulesLst = new ArrayList<>();
        rulesLst.add(fareRule);
        Rules rules = new Rules(rulesLst);

        CardFare fareCard = new CardFare(fares,  rules);

        JourneyTime journeyTime = new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        Journey journey = null;

        journey = new Journey("Zone 1", "Zone 1",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(10, 20)));

        rules.evaluate(journey);
        assertEquals(25, journey.getFare());
    }

    /**
     * E2E Test ,  normal flow for 5 entries through loop for a day
     */
    public void testRulesForNJourneys() {

        Fares fares = new Fares();
        FareRate fareRateZ1ToZ1 = new FareRate();
        fareRateZ1ToZ1.setFare(FareTimeType.PEAK, 30);
        fareRateZ1ToZ1.setFare(FareTimeType.NON_PEAK, 25);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.DAILY, 100);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.WEEKLY, 500);
        fares.addFare("Zone 1", "Zone 1", fareRateZ1ToZ1);

        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        duration.add(new RuleDuration(LocalTime.of(17, 0),
                LocalTime.of(20, 00), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));

        duration.add(new RuleDuration(LocalTime.of(9, 0),
                LocalTime.of(11, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
        duration.add(new RuleDuration(LocalTime.of(18, 0),
                LocalTime.of(22, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));

        FareRule fareRule = new FareRule(fares);

        List<IRule> rulesLst = new ArrayList<>();
        rulesLst.add(new FareRule(fares));
        rulesLst.add(new TimeRule(duration));
        rulesLst.add(fareRule);

        Rules rules = new Rules(rulesLst);


        WeekFields weekFields = WeekFields.of(Locale.getDefault());

        Journey journey = null;
        List<Journey> journeys = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            journey = new Journey("Zone 1", "Zone 1",
                    new JourneyTime(LocalDateTime.now().
                            with(weekFields.dayOfWeek(),
                                    DayOfWeek.MONDAY.getValue()).withHour(8+i).withMinute(0)));
            journeys.add(journey);
        }

        CardFare cardFare = new CardFare(fares,  rules);
        Map<Integer, Map<DayOfWeek, Integer>> journeyMap = new HashMap<>();
        Map<Integer, Integer> weekFareMap = new HashMap<>();
        int totalFareActual = cardFare.getFare(journeys, journeyMap, weekFareMap);
        assertEquals(35+35+30+0+0, totalFareActual);


    }

    /**
     * E2E Test , create 2 10 entries for 2 days (MONDAY, TUESDAY)
     */
    @Test
    public void testCase1E2E() {
        Fares fares = new Fares();
        FareRate fareRateZ1ToZ1 = new FareRate();
        fareRateZ1ToZ1.setFare(FareTimeType.PEAK, 30);
        fareRateZ1ToZ1.setFare(FareTimeType.NON_PEAK, 25);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.DAILY, 100);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.WEEKLY, 500);
        fares.addFare("Zone 1", "Zone 1", fareRateZ1ToZ1);


        FareRate fareRateZ1ToZ2 = new FareRate();
        fareRateZ1ToZ2.setFare(FareTimeType.PEAK, 35);
        fareRateZ1ToZ2.setFare(FareTimeType.NON_PEAK, 30);
        fareRateZ1ToZ2.setFareLimit(FareLimitType.DAILY, 120);
        fareRateZ1ToZ2.setFareLimit(FareLimitType.WEEKLY, 600);
        fares.addFare("Zone 1", "Zone 2", fareRateZ1ToZ2);


        fares.addFare("Zone 2", "Zone 1", fareRateZ1ToZ2);

        FareRate fareRateZ2ToZ2 = new FareRate();
        fareRateZ2ToZ2.setFare(FareTimeType.PEAK, 25);
        fareRateZ2ToZ2.setFare(FareTimeType.NON_PEAK, 20);
        fareRateZ2ToZ2.setFareLimit(FareLimitType.DAILY, 80);
        fareRateZ2ToZ2.setFareLimit(FareLimitType.WEEKLY, 400);
        fares.addFare("Zone 2", "Zone 2", fareRateZ2ToZ2);


        FareRule fareRule = new FareRule(fares);
        List<Journey> journeys1Day = new ArrayList<>();

        Journey journey;
        LocalDateTime time ;
        List<Journey> journeys2Day = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            time =   LocalDateTime.of(LocalDate.of(2021, Month.AUGUST, 10),
                    LocalTime.of(10, 20+i));
            journey = new Journey("Zone 1", "Zone 2",
                    new JourneyTime(time));
            journeys1Day.add(journey);
        }



        for (int i = 0; i < 10; i++) {
              time =   LocalDateTime.of(LocalDate.of(2021, Month.AUGUST, 11),
                    LocalTime.of(10, 20+i));

            journey = new Journey("Zone 1", "Zone 2",
                    new JourneyTime(time));
            journeys2Day.add(journey);
        }


        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        duration.add(new RuleDuration(LocalTime.of(17, 0),
                LocalTime.of(20, 00), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));

        duration.add(new RuleDuration(LocalTime.of(9, 0),
                LocalTime.of(11, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
        duration.add(new RuleDuration(LocalTime.of(18, 0),
                LocalTime.of(22, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));

        TimeRule timeRule = new TimeRule(duration);

        List<IRule> rulesLst = new ArrayList<>();

        rulesLst.add(timeRule);
        rulesLst.add(fareRule);

        Rules rules = new Rules(rulesLst);

        CardFare cardFare = new CardFare(fares, rules);
        int fare;


        ArrayList<Journey> journeys = new ArrayList<>();
        journeys.addAll(journeys1Day);
        journeys.addAll(journeys2Day);
        System.out.println(journeys);

        Map<Integer, Map<DayOfWeek, Integer>> journeyMap = new HashMap<>();
        Map<Integer, Integer> weekFareMap = new HashMap<>();
        fare = cardFare.getFare(journeys, journeyMap, weekFareMap);
        System.out.println(fare);
        assertEquals(240, fare);


    }

    /**
     * E2E Test , create 2 10 entries for 2 days (MONDAY, MONDAY)
     */
    @Test
    public void testCase2E2E() {
        Fares fares = new Fares();
        FareRate fareRateZ1ToZ1 = new FareRate();
        fareRateZ1ToZ1.setFare(FareTimeType.PEAK, 30);
        fareRateZ1ToZ1.setFare(FareTimeType.NON_PEAK, 25);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.DAILY, 100);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.WEEKLY, 500);
        fares.addFare("Zone 1", "Zone 1", fareRateZ1ToZ1);


        FareRate fareRateZ1ToZ2 = new FareRate();
        fareRateZ1ToZ2.setFare(FareTimeType.PEAK, 35);
        fareRateZ1ToZ2.setFare(FareTimeType.NON_PEAK, 30);
        fareRateZ1ToZ2.setFareLimit(FareLimitType.DAILY, 120);
        fareRateZ1ToZ2.setFareLimit(FareLimitType.WEEKLY, 600);
        fares.addFare("Zone 1", "Zone 2", fareRateZ1ToZ2);


        fares.addFare("Zone 2", "Zone 1", fareRateZ1ToZ2);

        FareRate fareRateZ2ToZ2 = new FareRate();
        fareRateZ2ToZ2.setFare(FareTimeType.PEAK, 25);
        fareRateZ2ToZ2.setFare(FareTimeType.NON_PEAK, 20);
        fareRateZ2ToZ2.setFareLimit(FareLimitType.DAILY, 80);
        fareRateZ2ToZ2.setFareLimit(FareLimitType.WEEKLY, 400);
        fares.addFare("Zone 2", "Zone 2", fareRateZ2ToZ2);


        FareRule fareRule = new FareRule(fares);
        List<Journey> journeys1Day = new ArrayList<>();

        Journey journey;
        LocalDateTime time ;
        List<Journey> journeys2Day = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            time =   LocalDateTime.of(LocalDate.of(2021, Month.AUGUST, 2),
                    LocalTime.of(10, 20+i));
            journey = new Journey("Zone 1", "Zone 2",
                    new JourneyTime(time));
            journeys1Day.add(journey);
        }



        for (int i = 0; i < 10; i++) {
            time =   LocalDateTime.of(LocalDate.of(2021, Month.AUGUST, 9),
                    LocalTime.of(10, 20+i));

            journey = new Journey("Zone 1", "Zone 2",
                    new JourneyTime(time));
            journeys2Day.add(journey);
        }


        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        duration.add(new RuleDuration(LocalTime.of(17, 0),
                LocalTime.of(20, 00), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));

        duration.add(new RuleDuration(LocalTime.of(9, 0),
                LocalTime.of(11, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
        duration.add(new RuleDuration(LocalTime.of(18, 0),
                LocalTime.of(22, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));

        TimeRule timeRule = new TimeRule(duration);

        List<IRule> rulesLst = new ArrayList<>();

        rulesLst.add(timeRule);
        rulesLst.add(fareRule);

        Rules rules = new Rules(rulesLst);

        CardFare cardFare = new CardFare(fares, rules);
        int fare;


        ArrayList<Journey> journeys = new ArrayList<>();
        journeys.addAll(journeys1Day);
        journeys.addAll(journeys2Day);
        System.out.println(journeys);

        Map<Integer, Map<DayOfWeek, Integer>> journeyMap = new HashMap<>();
        Map<Integer, Integer> weekFareMap = new HashMap<>();
        fare = cardFare.getFare(journeys, journeyMap, weekFareMap);
        System.out.println(fare);
        assertEquals(240, fare);
    }

}