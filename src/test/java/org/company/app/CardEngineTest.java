package org.company.app;

import org.company.app.model.*;
import org.company.app.rule.FareRule;
import org.company.app.rule.IRule;
import org.company.app.rule.RuleDuration;
import org.company.app.rule.TimeRule;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class CardEngineTest {
    /**
     * E2E Test
     */
    @Test
    public void testBlankJourney() {
        Fares fares = new Fares();
        List<Journey> journeys = new ArrayList<>();
        List<IRule> rules = new ArrayList<>();
        CardEngine engine = new CardEngine(fares, journeys, rules);
        assertEquals(0, engine.getFare());
    }

    /*
     * 1. Test Case pdf 78 Weekly calculation
     * 2. API - re-usability
     * */

    /**
     * E2E Test , single journey
     */
    @Test
    public void testSingleJourney() {
        Fares fares = new Fares();
        FareRate fareRate = new FareRate();
        fareRate.setFare(FareTimeType.PEAK, 30);
        fareRate.setFare(FareTimeType.NON_PEAK, 25);
        fares.addFare("Zone 1", "Zone 1", fareRate);
        FareRule fareRule = new FareRule(fares);
        List<Journey> journeys = new ArrayList<>();

        JourneyTime journeyTime = new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        Journey journey = new Journey("Zone 1", "Zone 1", journeyTime);
        journeys.add(journey);

        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        TimeRule timeRule = new TimeRule(duration);

        List<IRule> rules = new ArrayList<>();
        rules.add(timeRule);
        rules.add(fareRule);


        CardEngine engine = new CardEngine(fares, journeys, rules);
        int fare = engine.getFare();
        assertTrue(journey.getJourneyTime().isPeakHour());
        assertEquals(30, fare);
    }

    /**
     * E2E Test , single journey using not peak rule time.
     */
    @Test
    public void testSingleJourneyAtNonPeakTime() {
        Fares fares = new Fares();
        FareRate fareRate = new FareRate();
        fareRate.setFare(FareTimeType.PEAK, 30);
        fareRate.setFare(FareTimeType.NON_PEAK, 25);
        fares.addFare("Zone 1", "Zone 1", fareRate);
        FareRule fareRule = new FareRule(fares);
        List<Journey> journeys = new ArrayList<>();

        JourneyTime journeyTime = new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        Journey journey = new Journey("Zone 1", "Zone 1", journeyTime);
        journeys.add(journey);

        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        TimeRule timeRule = new TimeRule(duration, FareTimeType.NON_PEAK);

        List<IRule> rules = new ArrayList<>();
        rules.add(timeRule);
        rules.add(fareRule);


        CardEngine engine = new CardEngine(fares, journeys, rules);
        int fare = engine.getFare();
        assertFalse(journey.getJourneyTime().isPeakHour());
        assertEquals(25, fare);
    }

    /**
     * E2E Test , peak daily journey
     */
    @Test
    public void testPeakDailyJourney() {
        Fares fares = new Fares();
        FareRate fareRateZ1ToZ1 = new FareRate();
        fareRateZ1ToZ1.setFare(FareTimeType.PEAK, 30);
        fareRateZ1ToZ1.setFare(FareTimeType.NON_PEAK, 25);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.DAILY, 100);
        fares.addFare("Zone 1", "Zone 1", fareRateZ1ToZ1);


        FareRate fareRateZ1ToZ2 = new FareRate();
        fareRateZ1ToZ2.setFare(FareTimeType.PEAK, 35);
        fareRateZ1ToZ2.setFare(FareTimeType.NON_PEAK, 30);
        fareRateZ1ToZ2.setFareLimit(FareLimitType.DAILY, 120);
        fares.addFare("Zone 1", "Zone 2", fareRateZ1ToZ2);


        fares.addFare("Zone 2", "Zone 1", fareRateZ1ToZ2);

        FareRate fareRateZ2ToZ2 = new FareRate();
        fareRateZ2ToZ2.setFare(FareTimeType.PEAK, 25);
        fareRateZ2ToZ2.setFare(FareTimeType.NON_PEAK, 20);
        fareRateZ2ToZ2.setFareLimit(FareLimitType.DAILY, 80);
        fares.addFare("Zone 2", "Zone 2", fareRateZ2ToZ2);

        FareRule fareRule = new FareRule(fares);
        List<Journey> journeys = new ArrayList<>();


        Journey journey = new Journey("Zone 2", "Zone 1",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(10, 20)));
        journeys.add(journey);
        journeys.add(new Journey("Zone 1", "Zone 1",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(10, 45))));
        journeys.add(new Journey("Zone 1", "Zone 1",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(16, 15))));
        journeys.add(new Journey("Zone 1", "Zone 1",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(18, 15))));
        journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(19, 15))));

        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        duration.add(new RuleDuration(LocalTime.of(17, 0),
                LocalTime.of(20, 00), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));

        duration.add(new RuleDuration(LocalTime.of(9, 0),
                LocalTime.of(11, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
        duration.add(new RuleDuration(LocalTime.of(18, 0),
                LocalTime.of(22, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));

        TimeRule timeRule = new TimeRule(duration);

        List<IRule> rules = new ArrayList<>();
        rules.add(timeRule);
        rules.add(fareRule);


        CardEngine engine = new CardEngine(fares, journeys, rules);
        int fare = engine.getFare();
        assertTrue(journey.getJourneyTime().isPeakHour());
        assertEquals(35 + 25 + 25 + 30 + (5), fare);
    }

//    /**
//     * E2E Test , peak daily journey
//     */
//    @Test
//    public void testPeakDailyThroughLoops() {
//        Fares fares = new Fares();
//        FareRate fareRateZ1ToZ1 = new FareRate();
//        fareRateZ1ToZ1.setFare(FareTimeType.PEAK, 30);
//        fareRateZ1ToZ1.setFare(FareTimeType.NON_PEAK, 25);
//        fareRateZ1ToZ1.setFareLimit(FareLimitType.DAILY, 100);
//        fareRateZ1ToZ1.setFareLimit(FareLimitType.WEEKLY, 200);
//        fares.addFare("Zone 1", "Zone 1", fareRateZ1ToZ1);
//
//
//        FareRate fareRateZ1ToZ2 = new FareRate();
//        fareRateZ1ToZ2.setFare(FareTimeType.PEAK, 35);
//        fareRateZ1ToZ2.setFare(FareTimeType.NON_PEAK, 30);
//        fareRateZ1ToZ2.setFareLimit(FareLimitType.DAILY, 120);
//        fares.addFare("Zone 1", "Zone 2", fareRateZ1ToZ2);
//
//
//        fares.addFare("Zone 2", "Zone 1", fareRateZ1ToZ2);
//
//        FareRate fareRateZ2ToZ2 = new FareRate();
//        fareRateZ2ToZ2.setFare(FareTimeType.PEAK, 25);
//        fareRateZ2ToZ2.setFare(FareTimeType.NON_PEAK, 20);
//        fareRateZ2ToZ2.setFareLimit(FareLimitType.DAILY, 80);
//        fares.addFare("Zone 2", "Zone 2", fareRateZ2ToZ2);
//
//        FareRule fareRule = new FareRule(fares);
//        List<Journey> journeys1Day = new ArrayList<>();
//
//        Journey journey;
//        for (int i = 0; i < 10; i++) {
//            journey = new Journey("Zone 1", "Zone 1",
//                    new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(10, 20)));
//            journeys1Day.add(journey);
//        }
//
//        List<Journey> journeys2Day = new ArrayList<>();
//
//        for (int i = 0; i < 10; i++) {
//            journey = new Journey("Zone 2", "Zone 1",
//                    new JourneyTime(DayOfWeek.WEDNESDAY, LocalTime.of(10, 20)));
//            journeys2Day.add(journey);
//        }
//
//
//        List<RuleDuration> duration = new ArrayList<>();
//        duration.add(new RuleDuration(LocalTime.of(7, 0),
//                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
//        duration.add(new RuleDuration(LocalTime.of(17, 0),
//                LocalTime.of(20, 00), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
//
//        duration.add(new RuleDuration(LocalTime.of(9, 0),
//                LocalTime.of(11, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
//        duration.add(new RuleDuration(LocalTime.of(18, 0),
//                LocalTime.of(22, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
//
//        TimeRule timeRule = new TimeRule(duration);
//
//        List<IRule> rules = new ArrayList<>();
//        rules.add(timeRule);
//        rules.add(fareRule);
//
//
//        CardEngine engine = new CardEngine(fares, journeys1Day, rules);
//        int fare = engine.getFare(FareLimitType.WEEKLY);
//
//        assertEquals(120, fare);
//    }

    /**
     * E2E Test , peak weekly journey Code moved to another test suite
     */
    @Deprecated
    @Test
    public void testPeakWeeklyJourney() {
        Fares fares = new Fares();
        FareRate fareRateZ1ToZ1 = new FareRate();
        fareRateZ1ToZ1.setFare(FareTimeType.PEAK, 30);
        fareRateZ1ToZ1.setFare(FareTimeType.NON_PEAK, 25);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.DAILY, 100);
        fareRateZ1ToZ1.setFareLimit(FareLimitType.WEEKLY, 500);
        fares.addFare("Zone 1", "Zone 1", fareRateZ1ToZ1);


        FareRate fareRateZ1ToZ2 = new FareRate();
        fareRateZ1ToZ2.setFare(FareTimeType.PEAK, 35);
        fareRateZ1ToZ2.setFare(FareTimeType.NON_PEAK, 30);
        fareRateZ1ToZ2.setFareLimit(FareLimitType.DAILY, 120);
        fareRateZ1ToZ2.setFareLimit(FareLimitType.WEEKLY, 600);
        fares.addFare("Zone 1", "Zone 2", fareRateZ1ToZ2);


        fares.addFare("Zone 2", "Zone 1", fareRateZ1ToZ2);

        FareRate fareRateZ2ToZ2 = new FareRate();
        fareRateZ2ToZ2.setFare(FareTimeType.PEAK, 25);
        fareRateZ2ToZ2.setFare(FareTimeType.NON_PEAK, 20);
        fareRateZ2ToZ2.setFareLimit(FareLimitType.DAILY, 80);
        fareRateZ2ToZ2.setFareLimit(FareLimitType.WEEKLY, 400);
        fares.addFare("Zone 2", "Zone 2", fareRateZ2ToZ2);

        FareRule fareRule = new FareRule(fares);
        List<Journey> week1Journeys = new ArrayList<>();


        Journey journey = new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(10, 20)), 120);
        week1Journeys.add(journey);
        week1Journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.TUESDAY, LocalTime.of(10, 20)), 120));
        week1Journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.WEDNESDAY, LocalTime.of(10, 20)), 120));
        week1Journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.THURSDAY, LocalTime.of(10, 20)), 120));
        week1Journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.FRIDAY, LocalTime.of(10, 20)), 80));
        week1Journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.SATURDAY, LocalTime.of(10, 20)), 80)); //revised as 40
        week1Journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.SUNDAY, LocalTime.of(10, 20)), 80)); //revised as 0

        List<RuleDuration> duration = new ArrayList<>();
        duration.add(new RuleDuration(LocalTime.of(7, 0),
                LocalTime.of(10, 30), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));
        duration.add(new RuleDuration(LocalTime.of(17, 0),
                LocalTime.of(20, 00), DayOfWeek.MONDAY, DayOfWeek.FRIDAY));

        duration.add(new RuleDuration(LocalTime.of(9, 0),
                LocalTime.of(11, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
        duration.add(new RuleDuration(LocalTime.of(18, 0),
                LocalTime.of(22, 0), DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));


        List<IRule> rules = new ArrayList<>();


        List<Journey> week2Journeys = new ArrayList<>();
        week2Journeys.add(new Journey("Zone 1", "Zone 2",
                new JourneyTime(DayOfWeek.MONDAY, LocalTime.of(10, 20)), 120));


        CardEngine engine = new CardEngine(fares, week1Journeys, rules);
        int fare = engine.getFare(FareLimitType.WEEKLY);
        engine = new CardEngine(fares, week2Journeys, rules);
        fare += engine.getFare(FareLimitType.WEEKLY);

        assertEquals(120 + 120 + 120 + 120 + 80 + 40 + 0 + 120, fare);

    }
}

//Registered rules should not base on assumptions and the user invoke the method i, without rules

//Monday 2-2 journey 80 and next monday another journey 30 ,   60
//engine should not calculate the next monday as same day
// 4 trips 25 80 > 1st journey - journey - 80
// next monday 1 trip -> 1st journey - journey - 80 +25 -> output
