package org.company.app;

import junit.framework.TestCase;
import org.company.app.model.FareRate;
import org.company.app.model.FareTimeType;

/**
 * Test the fares
 */
public class FaresTest extends TestCase {

    /**
     * Adding a fares context
     */
    public void testAddFare() {
        Fares fares = new Fares();
        FareRate fareRate = new FareRate();
        fareRate.setFare(FareTimeType.PEAK, 30);
        fareRate.setFare(FareTimeType.NON_PEAK, 25);
        fares.addFare("Zone 1", "Zone 1", fareRate);
        assertEquals(Integer.valueOf(25), fares.getFare("Zone 1", "Zone 1").getFare(FareTimeType.NON_PEAK));
    }


}