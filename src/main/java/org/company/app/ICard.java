package org.company.app;

import org.company.app.model.Journey;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Map;

/**
 * Represents a card interface
 */
public interface ICard {
    /**
     * Get fare
     *
     * @param journeys journey/s
     * @param journeyMap based on weekOfYear and DayOfWeek fare map
     * @param weekFareMap based on week fare map
     * @return fares for the journey.
     */
    public int getFare(List<Journey> journeys, Map<Integer, Map<DayOfWeek,
            Integer>> journeyMap, Map<Integer, Integer> weekFareMap);
}
