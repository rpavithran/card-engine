package org.company.app.model;

/**
 * Type of types of fares based on time.
 */
public enum FareTimeType {
    /**
     * The singleton instance for fare  at peak time.
     */
    PEAK,
    /**
     * The singleton instance for fare  at non peak time.
     */
    NON_PEAK
}
