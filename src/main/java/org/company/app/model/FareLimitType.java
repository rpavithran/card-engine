package org.company.app.model;

/**
 * Type of limit types of fares.
 */
public enum FareLimitType {
    /**
     * The singleton instance for the daily fare limit.
     */
    DAILY,
    /**
     * The singleton instance for the weekly fare limit.
     */
    WEEKLY
}
