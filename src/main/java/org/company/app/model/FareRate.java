package org.company.app.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a fare rate.
 *
 * @author Rohith
 * @version 1.0
 */
public class FareRate {
    /**
     * Fare limit rates.
     */
    private Map<FareLimitType, Integer> fareLimits;
    /**
     * Fare time rates.
     */
    private Map<FareTimeType, Integer> fares;

    /**
     * Fare rates
     */
    public FareRate() {
        fareLimits = new HashMap<>();
        fares = new HashMap<>();
    }

    /**
     * Get fare limit for a specific type
     *
     * @param limitType fare limit type.
     * @return get fare limit.
     */
    public Integer getFareLimit(FareLimitType limitType) {
        Integer fare = null;
        if (null != fareLimits.get(limitType)) {
            fare = fareLimits.get(limitType);
        }
        return fare;
    }

    /**
     * Get fare limit for a specific type
     *
     * @param limitType fare limit type.
     * @param fare      amount.
     */
    public void setFareLimit(FareLimitType limitType, Integer fare) {
        fareLimits.put(limitType, fare);
    }

    /**
     * Get fare limit for a specific type
     *
     * @param timeType get time type
     * @return get the fare based on time type.
     */
    public Integer getFare(FareTimeType timeType) {
        return fares.get(timeType);
    }

    /**
     * Get fare limit for a specific type
     *
     * @param timeType fare.
     * @param fare     amount.
     */
    public void setFare(FareTimeType timeType, Integer fare) {
        fares.put(timeType, fare);
    }


}
