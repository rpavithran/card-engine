package org.company.app.model;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Represents a journey time
 */
public class JourneyTime {
    /**
     * Represents a journey time
     *
     * @since 1.1
     */
    private LocalDateTime dateTime;

    /**
     * Represents a journey time
     */
    private LocalTime time;
    /**
     * Represents a the day of week.
     */
    private DayOfWeek day;
    /**
     * Represents if a  journey is made in peak or not.
     */
    private boolean peakHour;

    /**
     * Represents if a  journey.
     *
     * @param day  day of journey.
     * @param time time of travel.
     * @since 1.0
     */
    public JourneyTime(DayOfWeek day, LocalTime time) {
        this.day = day;
        this.time = time;
    }

    /**
     * Represents if a  journey.
     *
     * @param dateTime time of travel.
     * @since 1.1
     */
    public JourneyTime(LocalDateTime dateTime) {
        super();
        this.day = dateTime.getDayOfWeek();
        this.time = LocalTime.of(dateTime.getHour(), dateTime.getMinute());
        this.dateTime = dateTime;
    }

    /**
     * Represents time of travel.
     *
     * @return time of travel.
     */
    public LocalTime getTime() {
        return time;
    }

    /**
     * Get the  week of day for travel.
     *
     * @return week of day of travel.
     */
    public DayOfWeek getDay() {
        return day;
    }

    /**
     * Get the  travel is made in peak or not.
     *
     * @return if peak time travel or not.
     */
    public boolean isPeakHour() {
        return peakHour;
    }

    /**
     * Sets  the  travel is made in peak or not.
     *
     * @param peakHour set if travel is at  peak time or not.
     */
    public void setPeakHour(boolean peakHour) {
        this.peakHour = peakHour;
    }

    /**
     * Get the  travel is made in peak or not.
     *
     * @return if  time travel or not.
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    /**
     * Sets  the  travel is made in peak or not.
     *
     * @param dateTime set the travel time.
     */
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "JourneyTime{" +
                "dateTime=[" + dateTime +
                "], time=" + time +
                ", day=" + day +
                ", peakHour=" + peakHour +
                '}';
    }
}
