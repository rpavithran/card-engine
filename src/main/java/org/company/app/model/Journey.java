package org.company.app.model;

/**
 * Represents a journey
 */
public class Journey {

    /**
     * Starting location of journey.
     */
    private String from;
    /**
     * Ending location of journey.
     */
    private String to;
    /**
     * Fare of journey.
     */
    private int fare;

    /**
     * Fare of journey.
     */
    private int actualFare;

    /**
     * Fare of next journey.
     */
    private Journey next;

    /**
     * Time of journey.
     */
    private JourneyTime journeyTime;


    /**
     * Create a new fare Object
     *
     * @param from        start of journey
     * @param to          end of journey
     * @param journeyTime time of travel
     */
    public Journey(String from, String to, JourneyTime journeyTime) {
        this.from = from;
        this.to = to;
        this.journeyTime = journeyTime;
    }

    /**
     * Create a new fare Object
     *
     * @param from        start of journey
     * @param to          end of journey
     * @param journeyTime time of travel
     * @param fare        time of travel
     */
    public Journey(String from, String to, JourneyTime journeyTime, int fare) {
        this(from, to, journeyTime);
        this.fare = fare;
    }

    /**
     * Return the starting location
     *
     * @return starting location
     */
    public String getFrom() {
        return from;
    }

    /**
     * Return the end location
     *
     * @return ending location
     */
    public String getTo() {
        return to;
    }

    /**
     * Get the fare involved
     *
     * @return fare for the journey.
     */
    public int getFare() {
        return fare;
    }

    /**
     * Set the fare for the journey.
     *
     * @param fare fare for the journey.
     */
    public void setFare(int fare) {
        this.fare = fare;
    }

    /**
     * Return the next journey if defined.
     *
     * @return next journey.
     */
    public Journey getNext() {
        return next;
    }

    /**
     * Return the next journey if defined.
     *
     * @return journey time.
     */
    public JourneyTime getJourneyTime() {
        return journeyTime;
    }


    /**
     * Return the actual fare for the journey if defined.
     *
     * @return next fare.
     */
    public int getActualFare() {
        return actualFare;
    }

    /**
     * Set the actual fare for the journey.
     *
     * @param actualFare fare for the journey.
     */
    public void setActualFare(int actualFare) {
        this.actualFare = actualFare;
    }

    @Override
    public String toString() {
        return "Journey{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", fare=" + fare +
                '}';
    }
}
