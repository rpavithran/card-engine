package org.company.app;

import org.company.app.model.FareRate;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents an fare context.
 *
 * @author Rohith
 * @version 1.0
 */
public class Fares {
    /**
     * Represents the fares context in a map.
     */
    Map<String, Map<String, FareRate>> faresMap;

    /**
     * Create the blank fares context.
     */
    public Fares() {
        faresMap = new HashMap<>();
    }

    /**
     * Add a fare for travel from source to destination
     *
     * @param start starting zone name
     * @param end   ending zone name
     * @param fare  associated fare rate
     */
    public void addFare(String start, String end, FareRate fare) {
        Map<String, FareRate> destMap;
        if (faresMap.containsKey(start)) {
            destMap = faresMap.get(start);
        } else {
            destMap = new HashMap<>();
            faresMap.put(start, destMap);
        }
        destMap.put(end, fare);
    }

    /**
     * Creates an employee with the specified name.
     *
     * @param start starting station
     * @param end   ending location
     * @return fare rate of the zone.
     */
    public FareRate getFare(String start, String end) {
        FareRate fareRate = null;
        if (null != faresMap.get(start)
                && null != faresMap.get(start).get(end)) {
            fareRate = faresMap.get(start).get(end);
        }
        return fareRate;
    }
}
