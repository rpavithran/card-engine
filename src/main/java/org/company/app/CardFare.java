package org.company.app;

import org.company.app.model.FareLimitType;
import org.company.app.model.Journey;
import org.company.app.rule.IRule;
import org.company.app.rule.Rules;

import java.time.DayOfWeek;
import java.time.temporal.WeekFields;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Represents a card on fare limit
 */
public class CardFare   implements  ICard {

    /**
     * Fares context.
     */
    private final Fares fares;

    /**
     * Rules to be applied in context.
     */
    private final Rules rules;

    /**
     * Card App
     *
     * @param fares    fares context
     * @param rules    rules to be applied
     */
    public CardFare(Fares fares,  Rules rules) {
        this.fares = fares;
        this.rules = rules;
    }

    /**
     * Get fare , implementation does analaysis and gets total Fare from week map.
     *
     * @param journeys journey/s
     * @param journeyMap based on weekOfYear and DayOfWeek fare map
     * @param weekFareMap based on week fare map
     * @return fares for the journey.
     */
    public int getFare(List<Journey> journeys,  Map<Integer, Map<DayOfWeek,
            Integer>> journeyMap, Map<Integer, Integer> weekFareMap) {

        for (Journey journey : journeys) {
            for (IRule rule: rules) {
                rule.evaluate(journey);
            }
            updateJourneyMap(journey, journeyMap, weekFareMap);
        }

        int totalFare = 0;
        for (Integer fare:
             weekFareMap.values()) {
            totalFare+=fare;
        }
        return totalFare;
    }

    /**
     * Get fare map analysis.
     *
     * @param journey journey
     * @param journeyMap based on weekOfYear and DayOfWeek fare map
     * @param weekFareMap based on week fare map
     *
     */
    private void updateJourneyMap(Journey journey,  Map<Integer, Map<DayOfWeek,
            Integer>> journeyMap, Map<Integer, Integer> weekFareMap) {
        int dailyFareLimit ;
        int weeklyFairLimit;
        int dailyFare ;
        int weekFare ;

        WeekFields weekFields = WeekFields.of(Locale.getDefault());

        int weekOfYear = journey.getJourneyTime().getDateTime().get(weekFields.weekOfYear());
        DayOfWeek dayOfWeek = DayOfWeek.of(journey.getJourneyTime().getDateTime().get(weekFields.dayOfWeek()));

        int tmpFare ;
        if(journeyMap.containsKey(weekOfYear)) {
            Map<DayOfWeek, Integer> dayOfWeekMap = journeyMap.get(weekOfYear);
            tmpFare = journey.getFare();
            if(dayOfWeekMap.containsKey(dayOfWeek)) {
                dailyFare = dayOfWeekMap.get(dayOfWeek);
                dailyFareLimit = fares.getFare(journey.getFrom(), journey.getTo()).getFareLimit(FareLimitType.DAILY);
                if((dailyFare + tmpFare)  > dailyFareLimit ) {
                    tmpFare = (dailyFareLimit - dailyFare);
                }
                dailyFare+=tmpFare;
                if(dailyFare != dayOfWeekMap.get(dayOfWeek)) {
                    dayOfWeekMap.put(dayOfWeek, dailyFare);
                }
            }   else {
                dayOfWeekMap.put(dayOfWeek, journey.getFare());
                journeyMap.put(weekOfYear, dayOfWeekMap);
            }
            if(weekFareMap.containsKey(weekOfYear)) {
                weekFare = weekFareMap.get(weekOfYear);
                weeklyFairLimit  = fares.getFare(journey.getFrom(), journey.getTo()).getFareLimit(FareLimitType.WEEKLY);
                if((weekFare + tmpFare)  > weeklyFairLimit ) {
                    tmpFare = (weeklyFairLimit - weekFare);
                }
                weekFare+=tmpFare;
                weekFareMap.put(weekOfYear, weekFare);
            }
        } else {
            Map<DayOfWeek, Integer> dayOfWeekMap = new HashMap<>();
            dayOfWeekMap.put(dayOfWeek, journey.getFare());
            journeyMap.put(weekOfYear, dayOfWeekMap);
            weekFareMap.put(weekOfYear, journey.getFare());
        }
    }


}
