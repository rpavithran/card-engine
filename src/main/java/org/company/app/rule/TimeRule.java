package org.company.app.rule;

import org.company.app.model.FareTimeType;
import org.company.app.model.Journey;
import org.company.app.model.JourneyTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a journey time
 */
public class TimeRule extends Rules {

    /**
     * Rules based on durations.
     */
    private List<RuleDuration> durations;

    /**
     * Type of during (peak or not)
     */
    private FareTimeType type;

    /**
     * Represents a blank journey time
     */
    public TimeRule() {
        super();
        this.durations = new ArrayList<>();
    }

    /**
     * Represents a journey time
     *
     * @param durations existing durations defined
     */
    public TimeRule(List<RuleDuration> durations) {
        this(durations, FareTimeType.PEAK);
    }

    /**
     * Represents a journey time
     *
     * @param durations existing durations defined
     * @param type      type of duration (peak or not)
     */
    public TimeRule(List<RuleDuration> durations, FareTimeType type) {
        super();
        this.durations = durations;
        this.type = type;
    }

    /**
     * Represents a journey time  rules
     */
    @Override
    public void evaluate(Journey journey) {
        JourneyTime time = journey.getJourneyTime();
        for (RuleDuration peak : durations) {
            if (time.getDay().getValue() >= peak.getStartDayOfWeek().getValue()
                    && (time.getDay().getValue() <= peak.getEndDayOfWeek().getValue())) {
                if ((peak.getStartTime().equals(time.getTime())
                        || (peak.getStartTime().isBefore(time.getTime())) &&
                        peak.getEndTime().isAfter(time.getTime()))) {
                    if (this.type == FareTimeType.PEAK) {
                        time.setPeakHour(true);
                    } else {
                        time.setPeakHour(false);
                    }
                }
            }
        }
    }
}
