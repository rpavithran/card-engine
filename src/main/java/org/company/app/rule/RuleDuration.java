package org.company.app.rule;


import java.time.DayOfWeek;
import java.time.LocalTime;

/**
 * Represents a time duration
 */
public class RuleDuration {
    /**
     * Represents a start time
     */
    private LocalTime startTime;
    /**
     * Represents a end duration
     */
    private LocalTime endTime;
    /**
     * Represents a week day to start a duration.
     */
    private DayOfWeek startDayOfWeek;
    /**
     * Represents a week day to end a duration.
     */
    private DayOfWeek endDayOfWeek;

    /**
     * Represents a week day to start a duration.
     *
     * @param startTime      starting time of duration
     * @param endTime        ending time of duration
     * @param startDayOfWeek starting day of week of duration
     * @param endDayOfWeek   ending day of week  of duration
     */
    public RuleDuration(LocalTime startTime, LocalTime endTime, DayOfWeek startDayOfWeek, DayOfWeek endDayOfWeek) {
        this.startTime = startTime;
        this.startDayOfWeek = startDayOfWeek;
        this.endTime = endTime;
        this.endDayOfWeek = endDayOfWeek;
    }

    /**
     * @return end day of duration
     */
    public DayOfWeek getEndDayOfWeek() {
        return endDayOfWeek;
    }

    /**
     * @return start time of duration
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * @return end time of duration
     */
    public LocalTime getEndTime() {
        return endTime;
    }

    /**
     * @return end day of week of duration
     */
    public DayOfWeek getStartDayOfWeek() {
        return startDayOfWeek;
    }


}
