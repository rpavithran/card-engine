package org.company.app.rule;

import org.company.app.model.Journey;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Represents a multiple instance wrapper around all rules.
 */
public class Rules implements Iterable<IRule>, IRule {

    /**
     * List of rules.
     */
    private List<IRule> rules;

    /**
     * Represents a blank journey time
     */
    public Rules() {
        super();
        this.rules = new ArrayList<>();
    }

    /**
     * Represents a blank journey time
     * @param  rules multiple rules context
     */
    public Rules(List<IRule> rules) {
        super();
        this.rules = rules;
    }


    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<IRule> iterator() {
        return rules.iterator();
    }

    /**
     * Performs the given action for each element of the {@code Iterable}
     * until all elements have been processed or the action throws an
     * exception.  Actions are performed in the order of iteration, if that
     * order is specified.  Exceptions thrown by the action are relayed to the
     * caller.
     * <p>
     * The behavior of this method is unspecified if the action performs
     * side-effects that modify the underlying source of elements, unless an
     * overriding class has specified a concurrent modification policy.
     *
     * @param action The action to be performed for each element
     * @throws NullPointerException if the specified action is null
     * <p>The default implementation behaves as if:
     * <pre>{@code
     *     for (T t : this)
     *         action.accept(t);
     * }</pre>
     * @since 1.8
     */
    @Override
    public void forEach(Consumer<? super IRule> action) {
        Iterable.super.forEach(action);
    }

    /**
     * Creates a {@link Spliterator} over the elements described by this
     * {@code Iterable}.
     *
     * @return a {@code Spliterator} over the elements described by this
     * {@code Iterable}.
     *  The default implementation creates an
     * <em><a href="../util/Spliterator.html#binding">early-binding</a></em>
     * spliterator from the iterable's {@code Iterator}.  The spliterator
     * inherits the <em>fail-fast</em> properties of the iterable's iterator.
     * The default implementation should usually be overridden.  The
     * spliterator returned by the default implementation has poor splitting
     * capabilities, is unsized, and does not report any spliterator
     * characteristics. Implementing classes can nearly always provide a
     * better implementation.
     * @since 1.8
     */
    @Override
    public Spliterator<IRule> spliterator() {
        return Iterable.super.spliterator();
    }

    /**
     * Represents multiple rules to be applied on a journey.
     *
     * @param journey journey reference.
     * @since 1.0
     */
    @Override
    public void applyRule(Journey journey) {
        evaluate(journey);
    }

    /**
     * Evaluates a  journey for a specific rule implementation.
     *
     * @param journey journey reference.
     * @since 1.1
     */
    @Override
    public void evaluate(Journey journey) {
        for (IRule rule : rules) {
            rule.evaluate(journey);
        }
    }
}
