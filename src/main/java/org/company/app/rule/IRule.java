package org.company.app.rule;

import org.company.app.model.Journey;

/**
 * Represents a journey rules
 */
public interface IRule {
    /**
     * Represents multiple rules to be applied on a journey.
     *
     * @param journey journey reference.
     * @since 1.0
     */
    @Deprecated
    void applyRule(Journey journey);

    /**
     * Evaluates a  journey for a specific rule implementation.
     *
     * @param journey journey reference.
     * @since 1.1
     */
    void evaluate(Journey journey);
}
