package org.company.app.rule;

import org.company.app.Fares;
import org.company.app.model.FareTimeType;
import org.company.app.model.Journey;

/**
 * Represents a rule on fare based on peak journey time
 */
public class FareRule implements IRule {

    /**
     * Fares  based on durations.
     */
    private Fares fares;

    /**
     * Represents a journey rule based on existing fares.
     *
     * @param fares existing fares context
     */
    public FareRule(Fares fares) {
        super();
        this.fares = fares;
    }

    /**
     * Represents a fare time rules
     */
    @Override
    public void applyRule(Journey journey) {
        evaluate(journey);
    }

    /**
     * Evaluates a  journey for a specific rule implementation.
     *
     * @param journey journey reference.
     * @since 1.1
     */
    @Override
    public void evaluate(Journey journey) {
        FareTimeType type;
        if (journey.getJourneyTime().isPeakHour()) {
            type = FareTimeType.PEAK;
        } else {
            type = FareTimeType.NON_PEAK;
        }
        journey.setFare(fares.getFare(journey.getFrom(), journey.getTo()).getFare(type));
    }
}
