package org.company.app;

import org.company.app.model.FareLimitType;
import org.company.app.model.FareRate;
import org.company.app.model.Journey;
import org.company.app.rule.IRule;

import java.util.List;

/**
 * Card App
 */
@Deprecated
public class CardEngine {

    /**
     * Fares context.
     */
    private final Fares fares;
    /**
     * Journey made during a duration.
     */
    private List<Journey> journeys;
    /**
     * Rules to be applied in context.
     */
    private List<IRule> rules;

    /**
     * Card App
     *
     * @param fares    fares context
     * @param journeys fares context
     * @param rules    fares context
     */
    public CardEngine(Fares fares, List<Journey> journeys, List<IRule> rules) {
        this.fares = fares;
        this.journeys = journeys;
        this.rules = rules;
    }

    /**
     * Get fare
     *
     * @return fares for the journey.
     */
    public int getFare() {
        return getFare(FareLimitType.DAILY);
    }

    /**
     * Get fare
     *
     * @param type limit type used for calculation
     * @return fares for the journey.
     */
    public int getFare(FareLimitType type) {
        int fare = 0;
        FareRate fareRate;
        Integer fareLimit = 0;
        int tmpJourneyFare = 0;

        for (Journey journey : journeys) {
            for (IRule rule : rules) {
                rule.applyRule(journey);
            }
            fareRate = fares.getFare(journey.getFrom(), journey.getTo());
            tmpJourneyFare = journey.getFare();
            if (null != fareRate) {
                fareLimit = fareRate.getFareLimit(type);
                if (null != fareLimit) {
                    if (fareLimit <= (tmpJourneyFare + fare)) {
                        tmpJourneyFare = (fareLimit - fare);
                    }
                }
            }
            fare += tmpJourneyFare;
        }
        return fare;
    }

}
